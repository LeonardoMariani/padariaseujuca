﻿namespace Padaria_Seu_Juca
{
    partial class inicioView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.buttonChefe = new System.Windows.Forms.Button();
            this.buttonCliente = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "O que deseja fazer?";
            // 
            // buttonChefe
            // 
            this.buttonChefe.Location = new System.Drawing.Point(15, 48);
            this.buttonChefe.Name = "buttonChefe";
            this.buttonChefe.Size = new System.Drawing.Size(247, 36);
            this.buttonChefe.TabIndex = 1;
            this.buttonChefe.Text = "Logar";
            this.buttonChefe.UseVisualStyleBackColor = true;
            this.buttonChefe.Click += new System.EventHandler(this.buttonChefe_Click);
            // 
            // buttonCliente
            // 
            this.buttonCliente.Location = new System.Drawing.Point(15, 101);
            this.buttonCliente.Name = "buttonCliente";
            this.buttonCliente.Size = new System.Drawing.Size(247, 36);
            this.buttonCliente.TabIndex = 2;
            this.buttonCliente.Text = "Fazer encomenda";
            this.buttonCliente.UseVisualStyleBackColor = true;
            this.buttonCliente.Click += new System.EventHandler(this.buttonCliente_Click);
            // 
            // inicioView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 161);
            this.Controls.Add(this.buttonCliente);
            this.Controls.Add(this.buttonChefe);
            this.Controls.Add(this.label1);
            this.Name = "inicioView";
            this.Text = "Inicio";
            this.Load += new System.EventHandler(this.inicioView_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonChefe;
        private System.Windows.Forms.Button buttonCliente;
    }
}

