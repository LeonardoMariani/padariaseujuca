﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Padaria_Seu_Juca.View.Cliente
{
    public partial class totalCompraView : Form
    {
        public totalCompraView(int preco)
        {
            InitializeComponent();
            labelPreco.Text = preco.ToString();
        }

        private void buttonConcluido_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
