﻿using Padaria_Seu_Juca.Controller;
using Padaria_Seu_Juca.Model;
using System;
using System.Data;
using System.Windows.Forms;

namespace Padaria_Seu_Juca.View.Cliente
{
    public partial class escolherProdutoView : Form
    {
        clienteModel cliente = new clienteModel();
        encomendaModel encomenda;
        encomendaController encomendaController;
        produtoController produtoController;
        Form telaInicial;
        DataTable listaProdutos = new DataTable();
        bool okEncomenda = false;
        String precoProduto;
        int precoTotal;

        public escolherProdutoView(clienteModel cliente,Form telaInicial)
        {
            this.cliente = cliente;
            this.telaInicial = telaInicial;
            InitializeComponent();
            dateTimePicker1.MinDate = DateTime.Today.AddDays(1);
            listaProdutos = produtoController.popularGrid();
            popularComboBox();
        }

        private void textBoxQtd_KeyPress(object sender, KeyPressEventArgs e)
        {
            Ferramentas.liberarNumero(e);
        }

        private void buttonProximo_Click(object sender, EventArgs e)
        {
            if (comboBoxProduto.SelectedIndex != -1 && textBoxQtd.Text != String.Empty)
            {
                criarModeloEncomenda();
                bool okQuantidade = false;
                okQuantidade = encomendaController.verificarQuantidade(encomenda);

                if(okQuantidade)
                {
                    encomendaController.atualizarEstoque(encomenda);
                    encomendaController = new encomendaController();
                    okEncomenda = encomendaController.novaEncomenda(cliente, encomenda);
                    if(okEncomenda)
                    {
                        produtoController = new produtoController();
                        precoProduto = produtoController.getPreco(comboBoxProduto.Text);
                        precoTotal = Convert.ToInt16(precoProduto) * Convert.ToInt16(textBoxQtd.Text);
                        totalCompraView tela = new totalCompraView(precoTotal);
                        this.Close();
                        tela.ShowDialog();

                    }
                }else
                {
                    MessageBox.Show("Quantidade maior do que a existente em estoque, por favor digite um valor menor.");
                }                
            }
            else
                MessageBox.Show("Existem campos não inseridos");
        }
        private void criarModeloEncomenda()
        {
            encomenda = new encomendaModel();
            encomenda.dataEntrega = dateTimePicker1.Value;
            encomenda.Produto = comboBoxProduto.Text;
            encomenda.Quantidade = Convert.ToInt16(textBoxQtd.Text);
        }
        private void popularComboBox()
        {
            comboBoxProduto.DisplayMember = "nome";
            comboBoxProduto.DataSource = listaProdutos;
        }
        private void buttonCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
            telaInicial.Show();
        }

        private void comboBoxProduto_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBoxQtd.Clear();
        }

        private void textBoxQtd_Leave(object sender, EventArgs e)
        {

        }

        private void textBoxQtd_KeyUp(object sender, KeyEventArgs e)
        {

            if (textBoxQtd.Text != "0" && textBoxQtd.Text != String.Empty)
                buttonProximo.Enabled = true;
            else
                buttonProximo.Enabled = false;
        }
    }
}
