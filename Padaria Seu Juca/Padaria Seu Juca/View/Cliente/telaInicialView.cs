﻿using Padaria_Seu_Juca.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Padaria_Seu_Juca.View.Cliente
{
    public partial class telaInicialView : Form
    {
        private Form telaAnterior;
        public telaInicialView(Form telaAnterior)
        {
            this.telaAnterior = telaAnterior;
            InitializeComponent();
        }

        private void textBoxTelefone_KeyPress(object sender, KeyPressEventArgs e)
        {
            Ferramentas.liberarNumero(e);
        }

        private void buttonCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
            telaAnterior.Show();
        }

        private void buttonProximo_Click(object sender, EventArgs e)
        {
            if (textBoxNome.Text != String.Empty && textBoxTelefone.Text != String.Empty)
            {
                clienteModel modelo = new clienteModel();
                modelo.Nome = textBoxNome.Text;
                modelo.Numero = textBoxTelefone.Text;
                escolherProdutoView tela = new escolherProdutoView(modelo,telaAnterior);
                this.Close();
                tela.ShowDialog();
            }
            else
                MessageBox.Show("Preencha todos os campos.");
        }
    }
}
