﻿namespace Padaria_Seu_Juca.View.Cliente
{
    partial class totalCompraView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonConcluido = new System.Windows.Forms.Button();
            this.labelPreco = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(173, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "O preço total da sua compra foi de:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(40, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "R$";
            // 
            // buttonConcluido
            // 
            this.buttonConcluido.Location = new System.Drawing.Point(44, 64);
            this.buttonConcluido.Name = "buttonConcluido";
            this.buttonConcluido.Size = new System.Drawing.Size(161, 42);
            this.buttonConcluido.TabIndex = 2;
            this.buttonConcluido.Text = "Concluido";
            this.buttonConcluido.UseVisualStyleBackColor = true;
            this.buttonConcluido.Click += new System.EventHandler(this.buttonConcluido_Click);
            // 
            // labelPreco
            // 
            this.labelPreco.AutoSize = true;
            this.labelPreco.Location = new System.Drawing.Point(77, 38);
            this.labelPreco.Name = "labelPreco";
            this.labelPreco.Size = new System.Drawing.Size(35, 13);
            this.labelPreco.TabIndex = 3;
            this.labelPreco.Text = "label3";
            // 
            // totalCompraView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 124);
            this.Controls.Add(this.labelPreco);
            this.Controls.Add(this.buttonConcluido);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "totalCompraView";
            this.Text = "Custo total";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonConcluido;
        private System.Windows.Forms.Label labelPreco;
    }
}