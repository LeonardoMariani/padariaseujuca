﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Padaria_Seu_Juca.View;
using Padaria_Seu_Juca.View.Cliente;

namespace Padaria_Seu_Juca
{
    public partial class inicioView : Form
    {
        public inicioView()
        {
            InitializeComponent();
        }

        private void buttonChefe_Click(object sender, EventArgs e)
        {
            loginView tela = new loginView(this);
            this.Hide();
            tela.ShowDialog();
        }

        private void inicioView_Load(object sender, EventArgs e)
        {

        }

        private void buttonCliente_Click(object sender, EventArgs e)
        {
            telaInicialView tela = new telaInicialView(this);
            this.Hide();
            tela.ShowDialog();

        }
    }
}
