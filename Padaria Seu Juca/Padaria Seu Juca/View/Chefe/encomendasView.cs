﻿using Padaria_Seu_Juca.Controller;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Padaria_Seu_Juca.View.Chefe
{
    public partial class encomendasView : Form
    {
        private DataTable encomendas;
        private Form telaAnterior;

        public encomendasView(Form telaAnterior)
        {
            this.telaAnterior = telaAnterior;
            InitializeComponent();
            encomendas = new DataTable();            
            popularGrid();
        }
        private void popularGrid()
        {
            encomendas = encomendaController.verificarEncomendas();
            dataGridView1.DataSource = encomendas;
        }

        private void buttonCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
            telaAnterior.Show();
        }
    }
}
