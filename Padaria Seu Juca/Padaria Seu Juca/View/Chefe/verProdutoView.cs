﻿using Padaria_Seu_Juca.Controller;
using Padaria_Seu_Juca.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Padaria_Seu_Juca.View.Chefe
{
    public partial class verProdutoView : Form
    {
        private Form telaAnterior;
        DataTable listaProdutos = new DataTable();
        produtoModel modelo = new produtoModel();
        int linhaSelecionada;

        public verProdutoView(Form telaAnterior)
        {
            this.telaAnterior = telaAnterior;
            InitializeComponent();
            listaProdutos = produtoController.popularGrid();
            popularComboBox();
        }

        private void popularComboBox()
        {
            comboBoxProdutos.DisplayMember = "nome";
            comboBoxProdutos.DataSource = listaProdutos;
            textBoxNome.Text = listaProdutos.Rows[0].ItemArray[0].ToString();
            textBoxPreco.Text = listaProdutos.Rows[0].ItemArray[1].ToString();
            textBoxQtd.Text = listaProdutos.Rows[0].ItemArray[2].ToString();
        }
        private void atualizarTextBox(int linha)
        {
            this.linhaSelecionada = linha;
            textBoxNome.Text = listaProdutos.Rows[linhaSelecionada].ItemArray[0].ToString();
            textBoxPreco.Text = listaProdutos.Rows[linhaSelecionada].ItemArray[1].ToString();
            textBoxQtd.Text = listaProdutos.Rows[linhaSelecionada].ItemArray[2].ToString();
        }
        private void buttonCancelar_Click(object sender, EventArgs e)
        {
            telaAnterior.Show();
            this.Hide();
        }

        private void comboBoxProdutos_SelectedIndexChanged(object sender, EventArgs e)
        {
            linhaSelecionada = comboBoxProdutos.SelectedIndex;
            atualizarTextBox(linhaSelecionada);

        }
    }
}
