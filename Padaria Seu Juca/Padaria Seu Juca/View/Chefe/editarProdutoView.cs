﻿using MySql.Data.MySqlClient;
using Padaria_Seu_Juca.Controller;
using Padaria_Seu_Juca.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Padaria_Seu_Juca.View.Chefe
{
    public partial class editarProdutoView : Form
    {
        Form telaAnterior;
        DataTable listaProdutos = new DataTable();
        produtoModel modelo = new produtoModel();
        int linhaSelecionada;

        public editarProdutoView(Form telaAnterior)
        {
            this.telaAnterior = telaAnterior;
            InitializeComponent();
            listaProdutos = produtoController.popularGrid();
            popularComboBox();
        }
        private void textBoxQtd_KeyPress(object sender, KeyPressEventArgs e)
        {
            Ferramentas.liberarNumero(e);
        }

        private void textBoxPreco_KeyPress(object sender, KeyPressEventArgs e)
        {
            Ferramentas.liberarPreco(e);
        }

        private void buttonCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
            telaAnterior.Show();
        }

        private void editarProdutoView_FormClosed(object sender, FormClosedEventArgs e)
        {

        }
        private void popularComboBox()
        {
            comboBoxNomeAntigo.DisplayMember = "nome";
            comboBoxNomeAntigo.DataSource = listaProdutos;
            textBoxNome.Text = listaProdutos.Rows[0].ItemArray[0].ToString();
            textBoxPreco.Text = listaProdutos.Rows[0].ItemArray[1].ToString();
            textBoxQtd.Text = listaProdutos.Rows[0].ItemArray[2].ToString();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            linhaSelecionada = comboBoxNomeAntigo.SelectedIndex;
            atualizarTextBox(linhaSelecionada);

        }
        private void atualizarTextBox(int linha)
        {
            this.linhaSelecionada = linha;
            textBoxNome.Text = listaProdutos.Rows[linhaSelecionada].ItemArray[0].ToString();
            textBoxPreco.Text = listaProdutos.Rows[linhaSelecionada].ItemArray[1].ToString();
            textBoxQtd.Text = listaProdutos.Rows[linhaSelecionada].ItemArray[2].ToString();
        }
        private void buttonAlterar_Click(object sender, EventArgs e)
        {
            produtoModel modelo = new produtoModel();
            bool okAlterar = false;

            modelo.Nome = textBoxNome.Text;
            modelo.Preco = float.Parse(textBoxPreco.Text);
            modelo.Quantidade = Convert.ToInt16(textBoxQtd.Text);              

            okAlterar = produtoController.editarProduto(modelo, comboBoxNomeAntigo.Text);

            if(okAlterar)
            {
                MessageBox.Show("Produto alterado com sucesso");
                this.Close();
                telaAnterior.Show();
            }else
                MessageBox.Show("Erro ao alterar produto");
        }

        private void editarProdutoView_Load(object sender, EventArgs e)
        {

        }

        private void textBoxNome_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBoxQtd_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxPreco_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
