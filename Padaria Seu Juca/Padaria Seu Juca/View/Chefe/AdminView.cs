﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Padaria_Seu_Juca.View;
using Padaria_Seu_Juca.Controller;

namespace Padaria_Seu_Juca.View.Chefe
{
    public partial class AdminView : Form
    {
        List<String> listaProdutosAcabando = new List<string>();

        public AdminView()
        {
            InitializeComponent();
            listaProdutosAcabando = produtoController.getProdutosAcabando();
            for (int i = 0;i< listaProdutosAcabando.Count;i++)
                MessageBox.Show("O produto: " + listaProdutosAcabando[i] + " esta acabando.");            
            
        }

        private void buttonCadastrar_Click(object sender, EventArgs e)
        {
            cadastroProdutoView tela = new cadastroProdutoView(this);
            this.Hide();
            tela.Show();
        }

        private void buttonEditar_Click(object sender, EventArgs e)
        {
            editarProdutoView tela = new editarProdutoView(this);
            this.Hide();
            tela.Show();
        }

        private void buttonVerificar_Click(object sender, EventArgs e)
        {            
            verProdutoView tela = new verProdutoView(this);
            this.Hide();
            tela.Show();
        }

        private void buttonEncomendas_Click(object sender, EventArgs e)
        {
            encomendasView tela = new encomendasView(this);
            this.Hide();
            tela.Show();
        }
    }
}
