﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Padaria_Seu_Juca.View;
using Padaria_Seu_Juca.Controller;
using Padaria_Seu_Juca.Model;
using Padaria_Seu_Juca.View.Chefe;

namespace Padaria_Seu_Juca.View
{
    public partial class loginView : Form
    {
        private Form telaInicio;
        private bool okLogin = false;
        chefeModel modeloChefe = new chefeModel();

        public loginView(Form telaInicio)
        {
            this.telaInicio = telaInicio;
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            telaInicio.Show();
        }

        private void loginView_FormClosed(object sender, FormClosedEventArgs e)
        {
            telaInicio.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBoxMatricula.Text != String.Empty && textBoxSenha.Text != String.Empty)
            {
                modeloChefe.Matricula = textBoxMatricula.Text;
                modeloChefe.Senha = textBoxSenha.Text;
                okLogin = loginController.verificarLogin(modeloChefe);
                if (okLogin)
                {
                    this.Hide();
                    AdminView tela = new AdminView();                    
                    tela.ShowDialog();
                }
                else
                    MessageBox.Show("Nem um usuario encontrado. Verifique os dados inseridos");
            }
            else
                MessageBox.Show("Preencha todos os campos");

        }
    }
}
