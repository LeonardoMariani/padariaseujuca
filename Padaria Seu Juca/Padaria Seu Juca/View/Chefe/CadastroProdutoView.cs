﻿using Padaria_Seu_Juca.Controller;
using Padaria_Seu_Juca.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Padaria_Seu_Juca.View.Chefe
{
    public partial class cadastroProdutoView : Form
    {
        Form adminView;
        bool okAdd = false;
        produtoModel produto = new produtoModel();

        public cadastroProdutoView(AdminView AdminView)
        {
            this.adminView = AdminView;
            InitializeComponent();
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            Ferramentas.liberarNumero(e);
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            Ferramentas.liberarPreco(e);
        }

        private void buttonAdicionar_Click(object sender, EventArgs e)
        {
            if (textBoxNome.Text != String.Empty && textBoxPreco.Text != String.Empty && textBoxQtd.Text != String.Empty)
            { 
                produto.Nome = textBoxNome.Text;
                produto.Preco = float.Parse(textBoxPreco.Text);
                produto.Quantidade = Convert.ToInt16(textBoxQtd.Text);

                okAdd = produtoController.adicionarProduto(produto);
                if (okAdd)
                {
                    this.Close();
                    adminView.Show();
                    MessageBox.Show("Cadastrado");
                }                  
                else
                    MessageBox.Show("Erro ao cadastrar produto");
            }else
                MessageBox.Show("Preencha todos os campos");

        }

        private void buttonVoltar_Click(object sender, EventArgs e)
        {
            adminView.Show();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBoxNome.Clear();
            textBoxPreco.Clear();
            textBoxQtd.Clear();
        }

        private void cadastroProdutoView_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
