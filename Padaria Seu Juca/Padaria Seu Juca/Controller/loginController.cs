﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Padaria_Seu_Juca.Model;

namespace Padaria_Seu_Juca.Controller
{
    class loginController
    {
        public static bool verificarLogin(chefeModel chefe)
        {
            bool okUsr = false;
            chefeDAO controllerDAO = new chefeDAO();

            okUsr = controllerDAO.checkUsuario(chefe);

            return okUsr;
        }
    }
}
