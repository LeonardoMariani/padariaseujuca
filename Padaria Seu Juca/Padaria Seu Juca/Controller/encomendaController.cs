﻿using Padaria_Seu_Juca.DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Padaria_Seu_Juca.Model;

namespace Padaria_Seu_Juca.Controller
{
    class encomendaController
    {

        public static DataTable verificarEncomendas()
        {
            DataTable encomendas = new DataTable();
            encomendasDAO dao = new encomendasDAO();

            encomendas = dao.verificarEncomendas();

            return encomendas;
        }

        public bool novaEncomenda(clienteModel cliente, encomendaModel encomenda)
        {
            encomendasDAO dao = new encomendasDAO();
            bool okEncomenda = false;

            okEncomenda = dao.novaEncomenda(cliente, encomenda);

            return okEncomenda;
        }

        public static bool verificarQuantidade(encomendaModel encomenda)
        {
            encomendasDAO dao = new encomendasDAO();
            bool okQtd = false;

            okQtd = dao.verificarQuantidade(encomenda);

            return okQtd;
        }

        public static void atualizarEstoque(encomendaModel encomenda)
        {
            encomendasDAO dao = new encomendasDAO();
            dao.atualizarEstoque(encomenda);            
        }
    }
}
