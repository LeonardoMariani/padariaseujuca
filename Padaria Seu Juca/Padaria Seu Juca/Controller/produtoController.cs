﻿using Padaria_Seu_Juca.DAO;
using Padaria_Seu_Juca.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Padaria_Seu_Juca.Controller
{
    class produtoController
    {
        public static bool adicionarProduto(produtoModel produto)
        {
            bool okAdd = false;

            produtoDAO dao = new produtoDAO();

            okAdd = dao.adicionarProduto(produto);

            return okAdd;
        }
        public static DataTable popularGrid()
        {
            DataTable produto = new DataTable();
            produtoDAO dao = new produtoDAO();

            produto = dao.popularGrid();

            return produto;
        }

        internal static List<string> getProdutosAcabando()
        {
            List<String> listaProdutosAcabando = new List<string>();
            produtoDAO dao = new produtoDAO();

            listaProdutosAcabando = dao.getProdutosAcabando();

            return listaProdutosAcabando;
        }

        public static bool editarProduto(produtoModel produto, String nomeAntigo)
        {
            bool okEditar = false;
            produtoDAO dao = new produtoDAO();

            okEditar =  dao.editarProduto(produto, nomeAntigo);

            return okEditar;
        }
        public static String getPreco(String nome)
        {
            String preco;
            produtoDAO dao = new produtoDAO();

            preco = dao.getPreco(nome);

            return preco;
        }
    }
}
