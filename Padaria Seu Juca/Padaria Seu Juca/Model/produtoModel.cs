﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Padaria_Seu_Juca.Model
{
    class produtoModel
    {
        public String Nome { get; set; }
        public float Preco { get; set; }
        public int Quantidade { get; set; }
    }
}
