﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Padaria_Seu_Juca
{
    class Ferramentas
    {
        public static void liberarNumero(KeyPressEventArgs e)
        {
            //Libera somente numeros
            char ch = e.KeyChar;
            if (!Char.IsNumber(ch) && ch != 8)
                e.Handled = true;
        }
        public static void liberarLetra(KeyPressEventArgs e)
        {
            //Libera somente letras
            char ch = e.KeyChar;
            if (!Char.IsLetter(ch) && ch != 8 && ch != 32)
                e.Handled = true;
        }
        public static void liberarPreco(KeyPressEventArgs e)
        {
            //Libera somente numeros , virgula e backspace
            if (!(Char.IsNumber(e.KeyChar)) && !(e.KeyChar == (char)8) && !(e.KeyChar == (char)44))
                e.Handled = true;
        }
    }
}
