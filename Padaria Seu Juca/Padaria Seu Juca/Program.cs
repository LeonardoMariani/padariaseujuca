﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Padaria_Seu_Juca.View;
using Padaria_Seu_Juca.View.Chefe;

namespace Padaria_Seu_Juca
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new inicioView());
            //Application.Run(new AdminView());
        }
    }
}
