﻿using System;
using Padaria_Seu_Juca.Model;
using MySql.Data.MySqlClient;

namespace Padaria_Seu_Juca.Controller
{
    class chefeDAO
    {
        private static string senha = "Leo995877";//Senha para fazer o login no banco de dados MySql  
        private static String caminho = "SERVER=localhost;DATABASE=Padaria;UID=root;PASSWORD=" + senha;
        private static MySqlConnection conexao = new MySqlConnection(caminho);

        public bool checkUsuario(chefeModel chefe)
        {
            bool okUser = false;
            string query = "Select * from Chefe where matricula = @matricula and senha = @senha";

            MySqlCommand cmd = new MySqlCommand(query, conexao);
            conexao.Open();

            cmd.Parameters.AddWithValue("@matricula", chefe.Matricula);
            cmd.Parameters.AddWithValue("@senha", chefe.Senha);

            MySqlDataReader read = cmd.ExecuteReader();

            if (read.Read())
                okUser = true;

            conexao.Close();
            return okUser;
        }
    }
}
