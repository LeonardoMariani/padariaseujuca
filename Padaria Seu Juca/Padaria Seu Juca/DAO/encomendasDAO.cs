﻿using MySql.Data.MySqlClient;
using System;
using System.Data;
using Padaria_Seu_Juca.Model;

namespace Padaria_Seu_Juca.DAO
{
    class encomendasDAO
    {
        private static string senha = "Leo995877";//Senha para fazer o login no banco de dados MySql  
        private static String caminho = "SERVER=localhost;DATABASE=Padaria;UID=root;PASSWORD=" + senha;
        private static MySqlConnection conexao = new MySqlConnection(caminho);
        private static bool okEncomenda = true;

        public DataTable verificarEncomendas()
        {
            DataTable encomendas = new DataTable();

            string query = "select * from Encomendas";
            MySqlCommand cmd = new MySqlCommand(query, conexao);

            try
            {
                conexao.Open();

                MySqlDataAdapter adapter = new MySqlDataAdapter(cmd);
                adapter.Fill(encomendas);
            }
            catch
            {
                encomendas = null;
            }
            conexao.Close();
            return encomendas;
        }

        public bool novaEncomenda(clienteModel cliente, encomendaModel encomenda)
        {
            string query = "insert into Encomendas(produto,quantidade,dataEntrega,nomeCliente,telefoneCliente) values(@produto,@quantidade,@dataEntrega,@nomeCliente,@telefoneCliente)";
            MySqlCommand cmd = new MySqlCommand(query, conexao);

            try
            {
                conexao.Open();

                cmd.Parameters.AddWithValue("@produto", encomenda.Produto);
                cmd.Parameters.AddWithValue("@quantidade", encomenda.Quantidade);
                cmd.Parameters.AddWithValue("@dataEntrega", encomenda.dataEntrega);
                cmd.Parameters.AddWithValue("@nomeCliente", cliente.Nome);
                cmd.Parameters.AddWithValue("@telefoneCliente", cliente.Numero);

                cmd.ExecuteNonQuery();
            }
            catch
            {
                okEncomenda = false;
            }
            conexao.Close();

            return okEncomenda;
        }

        public void atualizarEstoque(encomendaModel encomenda)
        {
            int qtdEstoque;
            int qtdFinal;
            string queryUpdateProduto = "update Produto set qtdEstoque = @qtdEstoque where nome = @nome";

            try
            {

                qtdEstoque = retornarQuantidade(encomenda.Produto);

                conexao.Open();
                if (qtdEstoque != -1)
                {
                    qtdFinal = qtdEstoque - encomenda.Quantidade;
                    MySqlCommand cmdUpdate = new MySqlCommand(queryUpdateProduto, conexao);

                    cmdUpdate.Parameters.AddWithValue("@nome", encomenda.Produto);
                    cmdUpdate.Parameters.AddWithValue("@qtdEstoque", qtdFinal);

                    cmdUpdate.ExecuteNonQuery();
                }
            }
            catch
            {

            }
            conexao.Close();
        }

        public bool verificarQuantidade(encomendaModel encomenda)
        {
            bool okQtd = true;
            int qtdEstoque = 0;

            qtdEstoque = retornarQuantidade(encomenda.Produto);

            if (qtdEstoque < encomenda.Quantidade) 
                okQtd = false; 
            
            return okQtd;
        }
        private int retornarQuantidade(String nome)
        {
            int quantidade;

            string query = "select qtdEstoque from Produto where nome = @nome";
            try
            {
                conexao.Open();

                MySqlCommand cmd = new MySqlCommand(query, conexao);

                cmd.Parameters.AddWithValue("@nome", nome);
                MySqlDataReader retorno = cmd.ExecuteReader();

                retorno.Read();
                quantidade = Convert.ToInt16(retorno.GetValue(0));
            }
            catch(Exception e)
            {
                quantidade = -1;
            }
            conexao.Close();
            return quantidade;
        }
    }
}
