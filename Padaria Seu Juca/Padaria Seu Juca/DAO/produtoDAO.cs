﻿using MySql.Data.MySqlClient;
using Padaria_Seu_Juca.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Padaria_Seu_Juca.DAO
{
    class produtoDAO
    {
        private static string senha = "Leo995877";//Senha para fazer o login no banco de dados MySql  
        private static String caminho = "SERVER=localhost;DATABASE=Padaria;UID=root;PASSWORD=" + senha;
        private static MySqlConnection conexao = new MySqlConnection(caminho);

        public bool adicionarProduto(produtoModel produto)
        {
            bool okAdd = true;
            string query = "insert into Produto(nome,preco,qtdEstoque) values(@nome,@preco,@qtdEstoque)";
            try
            {
                conexao.Open();
                MySqlCommand comando = new MySqlCommand(query, conexao);

                comando.Parameters.AddWithValue("@nome", produto.Nome);
                comando.Parameters.AddWithValue("@preco", produto.Preco);
                comando.Parameters.AddWithValue("@qtdEstoque", produto.Quantidade);

                comando.ExecuteNonQuery();
            }
            catch
            {
                okAdd = false;
            }
            conexao.Close();
            return okAdd;
        }

        internal List<string> getProdutosAcabando()
        {
            List<String> listaProdutosAcabando = new List<string>();
            string query = "select nome from Produto where qtdEstoque < 5";

            MySqlCommand comando = new MySqlCommand(query, conexao);

            try
            {
                conexao.Open();

                MySqlDataReader retorno = comando.ExecuteReader();
                if (retorno.HasRows)
                {
                    while(retorno.Read())
                    {
                        String nome = retorno["nome"].ToString();
                        listaProdutosAcabando.Add(nome);
                    }
                }               

            }
            catch
            {
                listaProdutosAcabando = null;
            }
            conexao.Close();

            return listaProdutosAcabando;
        }
        public string getPreco(string nome)
        {
            String preco;
            string query = "select preco from Produto where nome = @nome ";

            try
            {
                conexao.Open();

                MySqlCommand cmd = new MySqlCommand(query, conexao);

                cmd.Parameters.AddWithValue("@nome", nome);
                MySqlDataReader retorno = cmd.ExecuteReader();

                retorno.Read();
                preco = retorno.GetValue(0).ToString(); ;
            }
            catch (Exception e)
            {
                preco = "-1";
            }
            conexao.Close();

            return preco;
        }

        public DataTable popularGrid()
        {
            DataTable retorno = new DataTable();
            string query = "select * from Produto";
            MySqlCommand comando = new MySqlCommand(query, conexao);

            try
            {
                conexao.Open();

                MySqlDataAdapter adapter = new MySqlDataAdapter(comando);                
                adapter.Fill(retorno);
            }
            catch
            {
                retorno = null;
            }
            conexao.Close();

            return retorno;
        }
        public bool editarProduto(produtoModel modelo, String nomeAntigo)
        {
            bool okEditar = true;

            string query = "SET SQL_SAFE_UPDATES = 0;" +
                            "update Produto set nome = @novoNome, preco = @preco, qtdEstoque = @qtdEstoque where nome = @nomeAntigo;"+
                            "SET SQL_SAFE_UPDATES = 1 ";
            try{
                conexao.Open();

                MySqlCommand comando = new MySqlCommand(query, conexao);

                comando.Parameters.AddWithValue("@novoNome", modelo.Nome);
                comando.Parameters.AddWithValue("@preco", modelo.Preco);
                comando.Parameters.AddWithValue("@qtdEstoque", modelo.Quantidade);
                comando.Parameters.AddWithValue("@nomeAntigo", nomeAntigo);

                comando.ExecuteNonQuery();
            }
            catch
            {
                okEditar = false;
            }
            conexao.Close();
            return okEditar;
        }
    }
}
